package tv.blockgraph.mockcentral.tables;

import java.util.*;
import tv.blockgraph.mockcentral.tables.*;

//this class wraps a Hashtable that stores participant record objects mapped to the participant IP
//for fast lookup
public class LookupTable {

    private final Hashtable<Integer, ParticipantRecord> lookupTable;

    public LookupTable() {
        this.lookupTable = new Hashtable<>();
    }

    //inserts a new participant into the lookup table with permissions set
    public void insertRecord(int participant, String operation, List<Integer> participants, String ip, String kafkaEndpoint) {
        ParticipantRecord record = new ParticipantRecord(participant, ip, kafkaEndpoint);
        record.addPermissions(new HashSet(participants), operation);
        this.lookupTable.put(participant, record);
    }

    //Gets the participant that we are requesting and checks the permissions
    public boolean checkPermission(int from, String operation, int to) {
        try {
            return this.lookupTable.get(to).checkPermissions(from, operation);
        }
        catch (NullPointerException e) {
            return false;
        }
    }

    public ParticipantRecord getParticipantRecord(int participant) {
        if (this.lookupTable.get(participant) != null) {
            return this.lookupTable.get(participant);
        }
        else {
            return new ParticipantRecord(0, "0", "0");
        }
    }

    public List<ParticipantRecord> getAllParticipants() {
        ArrayList<ParticipantRecord> list = new ArrayList();
        for (int key : lookupTable.keySet()) {
            list.add(lookupTable.get(key));
        }
        return list;
    }

    public String getKafkaEndpoint(int participant) {
        try {
            return this.lookupTable.get(participant).getKafkaEndpoint();
        }
        catch (NullPointerException e) {
            return "null";
        }
    }
}
