package tv.blockgraph.mockcentral.tables;

import java.util.*;

//this class stores the participant data, including IP address, Kafka endpoint, and permissions config
public class ParticipantRecord {
    private int id;
    private String ip;
    private final Hashtable<String, HashSet<Integer>> operationPermissions;
    private String kafkaEndpoint;

    public ParticipantRecord(int id, String ip, String kafkaEndpoint) {
        this.id = id;
        this.ip = ip;
        this.kafkaEndpoint = kafkaEndpoint;
        this.operationPermissions = new Hashtable<>();
    }

    public ParticipantRecord(int id, String ip, String kafkaEndpoint, Hashtable<String, HashSet<Integer>> perms) {
        this.id = id;
        this.ip = ip;
        this.kafkaEndpoint = kafkaEndpoint;
        this.operationPermissions = perms;
    }

    public boolean checkPermissions(int participant, String operation) {
        try {
            return this.operationPermissions.get(operation).contains(participant);
        }
        catch (NullPointerException e) {
            return false;
        }
    }

    public void addPermissions(HashSet<Integer> participants, String operation) {
        this.operationPermissions.put(operation, participants);
    }

    public int getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public String getKafkaEndpoint() {
        return kafkaEndpoint;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setKafkaEndpoint(String kafkaEndpoint) {
        this.kafkaEndpoint = kafkaEndpoint;
    }
}
