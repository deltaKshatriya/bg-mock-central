package tv.blockgraph.mockcentral.service;

import tv.blockgraph.mockcentral.access.AccessResponse;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import org.springframework.stereotype.Service;
import tv.blockgraph.mockcentral.participant_data.ParticipantObj;
import tv.blockgraph.mockcentral.tables.LookupTable;
import tv.blockgraph.mockcentral.tables.ParticipantRecord;
import tv.blockgraph.mockcentral.transactions.*;
import tv.blockgraph.mockcentral.controller.MockParticipantController;

@Service
public class AccessLookUpService {

    private final LookupTable lookup;
    private TransactionsLogger ledger;

    public AccessLookUpService() {
        this.ledger = new TransactionsLogger("ledgerLog");
        this.lookup = new LookupTable();
        this.lookup.insertRecord(0, "match", Arrays.asList(1, 2, 3, 4, 5), "localhost", "localhost:9092");
    }
    //Checks permissions from a lookup table class that wraps the internal participant lookup table
    public AccessResponse checkPermissions(int from, int to, String operation, MockParticipantController requestSystem) {
        if (lookup.checkPermission(from, operation, to)) {
            ledger.logTransaction(from, to, operation);
            String ip = lookup.getParticipantRecord(to).getIp();
            //now we want to let the target participant know that it's being requested
//            try {
//                requestSystem.receiveMatchRequest(new URI("http://" + ip + ":9920"), from);
//            }
//            catch (URISyntaxException e) {
//
//            }
        }
        return new AccessResponse(lookup.checkPermission(from, operation, to), lookup.getKafkaEndpoint(to), lookup.getParticipantRecord(to).getIp());
    }

    public List<ParticipantObj> getParticipants() {
        ArrayList<ParticipantObj> list = new ArrayList();
        for (ParticipantRecord record : this.lookup.getAllParticipants()){
            list.add(new ParticipantObj(Integer.toString(record.getId()),
                    record.getKafkaEndpoint()));
        }
        return list;
    }
}
