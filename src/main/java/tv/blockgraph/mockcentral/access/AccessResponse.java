package tv.blockgraph.mockcentral.access;

public class AccessResponse {

    private final boolean isAllowed;
    private final String kafkaPoint;
    private final String targetIP;

    public AccessResponse(boolean isAllowed, String kafkaPoint, String targetIP) {
        this.isAllowed = isAllowed;
        this.kafkaPoint = kafkaPoint;
        this.targetIP = targetIP;
    }

    public boolean getIsAllowed() {
        return this.isAllowed;
    }

    public String getKafkaPoint() {
        return this.kafkaPoint;
    }

    public String getTargetIP() { return this.targetIP; }

}
