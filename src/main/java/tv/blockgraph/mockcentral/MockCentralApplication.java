package tv.blockgraph.mockcentral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MockCentralApplication {

    public static void main(String[] args) {
        SpringApplication.run(MockCentralApplication.class, args);
    }
}
