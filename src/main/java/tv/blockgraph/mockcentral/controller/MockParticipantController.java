package tv.blockgraph.mockcentral.controller;


import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@FeignClient(name = "ParticipantController")
public interface MockParticipantController {

    //@GetMapping(value = "/receive-match-request")
    @RequestLine("GET /receive-match-request/?from={from}")
    public void receiveMatchRequest(URI baseUri, @Param(value = "from") int from);
}
