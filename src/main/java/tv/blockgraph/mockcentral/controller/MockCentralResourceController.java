package tv.blockgraph.mockcentral.controller;

import feign.Feign;
import feign.Target;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.*;
import tv.blockgraph.mockcentral.access.AccessResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.swagger.annotations.ApiOperation;

import tv.blockgraph.mockcentral.participant_data.ParticipantObj;
import tv.blockgraph.mockcentral.service.AccessLookUpService;
import tv.blockgraph.mockcentral.controller.*;
import org.springframework.beans.factory.annotation.*;

import java.util.List;


//REST Controller for the central API
@RestController
@Import(FeignClientsConfiguration.class)
public class MockCentralResourceController {

    private final AccessLookUpService service;
    MockParticipantController requestEngine;


    @Autowired
    public MockCentralResourceController(AccessLookUpService service, Decoder decoder, Encoder encoder) {
        this.service = service;
        this.requestEngine = Feign.builder().encoder(encoder).decoder(decoder)
                .target(Target.EmptyTarget.create(MockParticipantController.class));
    }

    /*Checks if a requesting participant has access to perform a particular operation on
    * another participant. Returns true/false to the query, along with relevant endpoint information*/
    @GetMapping(value = "/checkAccessLine", produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Checks access control for two participants", notes = "")
    public AccessResponse checkAccessLine(
            @RequestParam(value = "from") int from,
            @RequestParam(value = "to") int to,
            @RequestParam(value = "operation") String operation) {
        return service.checkPermissions(from, to, operation, requestEngine);
    }
    @GetMapping(value = "/get-participants", produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Returns a list of participants in the network")
    public List<ParticipantObj> getParticipants() {
        return service.getParticipants();
    }
}
