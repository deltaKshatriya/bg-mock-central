package tv.blockgraph.mockcentral.participant_data;

public class ParticipantObj {
    private String participantId;
    private String p2pBrokers;

    public ParticipantObj(String participantId, String brokers) {
        this.participantId = participantId;
        this.p2pBrokers = brokers;
    }

    public String getParticipantId() {
        return participantId;
    }

    public String getP2pBrokers() {
        return p2pBrokers;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public void setP2pBrokers(String brokers) {
        this.p2pBrokers = brokers;
    }
}
