package tv.blockgraph.mockcentral.transactions;

import java.util.logging.*;
import java.io.*;

public class TransactionsLogger {
    private Logger logger;
    private FileHandler fh;

    public TransactionsLogger(String logFile) {
        logger = Logger.getLogger(logFile);
        try {
            fh = new FileHandler(logFile + ".log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.setUseParentHandlers(true);
            System.out.println("Transactions Ledger set up");
        }
        catch (IOException e) {
            logger.log(new LogRecord(Level.WARNING, "WARNING! FAILED TO INITIALIZE TRANSACTIONS LEDGER LOGGER!"));
        }
    }

    public void logTransaction(int from, int to, String operation) {
        logger.log(new LogRecord( Level.FINE, "TRANSACTION LOG OF OPERATION: " + operation + " FROM: " + from + " TO: " + to));
        logger.info("TRANSACTION LOG OF OPERATION: " + operation + " FROM: " + from + " TO: " + to);
    }
}
